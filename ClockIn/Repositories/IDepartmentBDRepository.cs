﻿using ClockIn.Models;
using ClockIn.Models.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClockIn.Repositories
{
    public interface IDepartmentBDRepository
    {
        Task<IEnumerable<DepartmentBranch>> DepartmentsFillListByCompanyID(int CompanyID);
        Task<string> AddNewDepartment(DepartmentViewModel departmen);
        Task<string> UpdateDepartment(DepartmentViewModel department);
        Task<string> DeleteDepartment(int id);
    }
}