﻿using ClockIn.Models;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using static ClockIn.DataAccessLayer.DataAccess;

namespace ClockIn.Repositories
{
    public class BranchBDRepository : IBranchBDRepository
    {
        public IDbConnection db = new SqlConnection(GetConnectionString());

        public async Task<IEnumerable<Branch>> BranchesFillListByCompanyID(int CompanyID) 
        {
            
            try
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@CompanyID", CompanyID);
                return await db.QueryAsync<Branch>("BranchesFillListByCompanyID", p, commandType: CommandType.StoredProcedure);
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public async Task<string> AddNewBranch(Branch branch)
        {
            string isComplete = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    DynamicParameters pCompany = new DynamicParameters();
                    pCompany.Add("@NameAr", branch.NameAr);
                    pCompany.Add("@NameEng", branch.NameAr); // ToDo Phone2
                    pCompany.Add("@Address", branch.Address);
                    pCompany.Add("@Emp_ID", branch.Emp_ID);
                    pCompany.Add("@Phone1", branch.Phone1);
                    pCompany.Add("@Phone2", branch.Phone1); // ToDo Phone2
                    pCompany.Add("@Email", branch.Email);
                    pCompany.Add("@Notes", branch.Notes);
                    pCompany.Add("@City_ID", branch.City_ID);
                    pCompany.Add("@SaveUser_ID", branch.SaveUser_ID);
                    pCompany.Add("@LastEditUser_ID", null);
                    pCompany.Add("@Company_ID", branch.Company_ID);
                    pCompany.Add("@Latitude", branch.Latitude);
                    pCompany.Add("@Longitude", branch.Longitude);

                    int BranchId = await db.QueryFirstOrDefaultAsync<int>("BranchesInsert", pCompany, commandType: CommandType.StoredProcedure);
                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                isComplete = ex.Message;
            }

            return isComplete;
        }

        public async Task<string> UpdateBranch(Branch branch)
        {
            string IsComplet = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    DynamicParameters pCompany = new DynamicParameters();
                    pCompany.Add("@ID", branch.ID);
                    pCompany.Add("@NameAr", branch.NameAr);
                    pCompany.Add("@NameEng", branch.NameAr); // ToDo Phone2
                    pCompany.Add("@Address", branch.Address);
                    pCompany.Add("@Emp_ID", branch.Emp_ID);
                    pCompany.Add("@Phone1", branch.Phone1);
                    pCompany.Add("@Phone2", branch.Phone1); // ToDo Phone2
                    pCompany.Add("@Email", branch.Email);
                    pCompany.Add("@Notes", branch.Notes);
                    pCompany.Add("@City_ID", branch.City_ID);
                    pCompany.Add("@SaveUser_ID", branch.SaveUser_ID);
                    pCompany.Add("@LastEditUser_ID", null);
                    pCompany.Add("@Company_ID", branch.Company_ID);
                    pCompany.Add("@Latitude", branch.Latitude);
                    pCompany.Add("@Longitude", branch.Longitude);

                    int BranchId = await db.QueryFirstOrDefaultAsync<int>("BranchesUpdate", pCompany, commandType: CommandType.StoredProcedure);
                    transaction.Complete();
                }

            }
            catch (SqlException ex)
            {
                IsComplet = ex.Message;
            }

            return IsComplet;
        }

        public async Task<string> DeleteBranch(int id)
        {
            string iscompletInsert = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    DynamicParameters pCompany = new DynamicParameters();
                    pCompany.Add("@ID", id);
                    await db.QueryAsync<int>("BranchesDelete", pCompany, commandType: CommandType.StoredProcedure);
                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                iscompletInsert = ex.Message;
            }

            return iscompletInsert;
        }

    }
}