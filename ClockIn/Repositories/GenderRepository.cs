﻿using ClockIn.Models;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using static ClockIn.DataAccessLayer.DataAccess;

namespace ClockIn.Repositories
{
    public class GenderRepository : IGenderRepository
    {
        public IDbConnection db = new SqlConnection(GetConnectionString());
        private readonly DynamicParameters param = new DynamicParameters();

        public async Task<IEnumerable<Gender>> GetAllGenderAsync()
        {
            return await db.QueryAsync<Gender>("GenderFillList", param, commandType: CommandType.StoredProcedure);
        }
    }
}