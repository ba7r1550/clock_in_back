﻿using ClockIn.DTO;
using ClockIn.Models;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Transactions;
using static ClockIn.DataAccessLayer.DataAccess;

namespace ClockIn.Repositories
{
    public class UserAccountRepository : IUserAccountRepository
    {
        public IDbConnection db = new SqlConnection(GetConnectionString());
        private readonly DynamicParameters param = new DynamicParameters();

        public async Task<IEnumerable<UserAccount>> GetAllUserAccountAsync()
        {
            return await db.QueryAsync<UserAccount>("UserAccountsSelectAll", param, commandType: CommandType.StoredProcedure);
        }

        public async Task<string> InsertUserAccountAsync(UserAccount employee)
        {
            string IscompletInsert = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    param.Add("@Emp_ID", employee.Emp_ID);
                    param.Add("@User_Name", employee.User_Name);
                    param.Add("@User_PWD", employee.User_PWD);
                    param.Add("@Enabled", employee.Enabled);

                    var result = await db.QueryFirstOrDefaultAsync<int>("UserAccountsInsert", param, commandType: CommandType.StoredProcedure);
                    IscompletInsert = result.ToString();

                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                IscompletInsert = ex.Message;
            }

            return IscompletInsert;
        }

        public async Task<UserAccount> GetUserAccountByIdAsync(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@ID", id);
            return await db.QueryFirstAsync<UserAccount>("UserAccountsSelectByEmpId", p, commandType: CommandType.StoredProcedure);
        }

        public async Task<UserLoginData> LoginUserAsync(UserAccountDto userAccountDto)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@sUserName", userAccountDto.User_Name);
            p.Add("@sPwd", userAccountDto.User_PWD);
            return await db.QueryFirstAsync<UserLoginData>("UserAccountsLogin", p, commandType: CommandType.StoredProcedure);
        }

        public async Task<UserLoginData> LoginUserPortalAsync(UserAccountDto userAccountDto)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@sUserName", userAccountDto.User_Name);
            p.Add("@sPwd", userAccountDto.User_PWD);
            return await db.QueryFirstAsync<UserLoginData>("UserAccountsPortalLogin", p, commandType: CommandType.StoredProcedure);
        }
        public async Task<UserLoginData> EmployeeGetAllData(int  EmpID)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@EmpID", EmpID);
            return await db.QueryFirstAsync<UserLoginData>("EmployeeGetAllData", p, commandType: CommandType.StoredProcedure);
        }

        /// //////////////////////////////////////
        public async Task<string> EmployeeChangPassword(changPass data)
        {
            string IsCopmlete = null;

            try
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@Password", data.EmpID);
                p.Add("@EmpID", data.Password);
                await db.QueryAsync<int>("EmployeeChangPassword", p, commandType: CommandType.StoredProcedure);
            }
            catch (SqlException ex)
            {
                IsCopmlete = ex.Message;
            }

            return IsCopmlete;
        }
        /////////////////////////////////////////
    }
}