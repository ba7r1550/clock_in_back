﻿using ClockIn.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Transactions;
using static ClockIn.DataAccessLayer.DataAccess;

namespace ClockIn.Repositories
{
    public class CompanyBDRepository : ICompanyBDRepository
    {
        public IDbConnection db = new SqlConnection(GetConnectionString());


        public async Task<string> RegisterCompany(RegisterNewCompany company, int UserName, int password)
        {
            string isComplete = null;
            string CustomCode = DateTime.Now.ToString("HHmmssFF");
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    
                    DynamicParameters pCompany = new DynamicParameters();
                    pCompany.Add("@NameAr", company.NameAr);
                    pCompany.Add("@NameEng", company.NameAr);
                    pCompany.Add("@Address", company.Address);
                    pCompany.Add("@Phone1", company.Phone1);
                    pCompany.Add("@Phone2", company.Phone2);
                    pCompany.Add("@CompType_ID", company.CompType_ID);
                    pCompany.Add("@Email", company.Email);
                    pCompany.Add("@Notes", company.Notes);
                    pCompany.Add("@City_ID", company.City_ID);
                    pCompany.Add("@Country_ID", company.Country_ID);
                    pCompany.Add("@SaveUser_ID", company.SaveUser_ID);
                    pCompany.Add("@User_Name", UserName);
                    pCompany.Add("@User_PWD", password);
                    pCompany.Add("@DepartmentCode", CustomCode);
                    int CompanyID = await db.QueryFirstOrDefaultAsync<int>("CompanyInsert", pCompany, commandType: CommandType.StoredProcedure);
                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                isComplete = ex.Message;
            }

            return isComplete;
        }
    }
}