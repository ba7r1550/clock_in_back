﻿using ClockIn.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClockIn.Repositories
{
    public interface IGenderRepository
    {
        Task<IEnumerable<Gender>> GetAllGenderAsync();
    }
}