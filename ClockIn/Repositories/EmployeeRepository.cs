﻿using ClockIn.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Transactions;
using static ClockIn.DataAccessLayer.DataAccess;

namespace ClockIn.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        public IDbConnection db = new SqlConnection(GetConnectionString());
        private readonly DynamicParameters param = new DynamicParameters();

        public async Task<IEnumerable<Employee>> GetAllEmployeeAsync()
        {
            return await db.QueryAsync<Employee>("EmployeesSelectAll", param, commandType: CommandType.StoredProcedure);
        }

        public async Task<string> InsertEmployeeAsync(Employee employee)
        {
            string IscompletInsert = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    param.Add("@NameAr", employee.NameAr);
                    param.Add("@NameEng", employee.NameEng);
                    param.Add("@Gender_ID", employee.Gender_ID);
                    param.Add("@IDNo", employee.IDNo); // Validation
                    param.Add("@Address", employee.Address);
                    param.Add("@Tel", employee.Tel);
                    param.Add("@Mobile", employee.Mobile); // Validation
                    param.Add("@Email", employee.Email); // Validation
                    param.Add("@Job_ID", employee.Job_ID);
                    param.Add("@Salary", employee.Salary);
                    param.Add("@Dep_ID", employee.Dep_ID);
                    param.Add("@SocialID", employee.SocialID);
                    param.Add("@Marital_Status", employee.Marital_Status);
                    param.Add("@Millitary_ID", employee.Millitary_ID);
                    param.Add("@Enabled", employee.Enabled);
                    param.Add("@QualifType_ID", employee.QualifType_ID);
                    param.Add("@BirthDate", employee.BirthDate);
                    param.Add("@Employment_Date", employee.Employment_Date);
                    param.Add("@GraduateYear", employee.GraduateYear);
                    param.Add("@City_ID", employee.City_ID);
                    param.Add("@Notes", employee.Notes);
                    param.Add("@DriverLicense", employee.DriverLicense);
                    param.Add("@DriverLicense_Type", employee.DriverLicense_Type);
                    param.Add("@DriverLicenseNo", employee.DriverLicenseNo);
                    param.Add("@Branch_ID", employee.Branch_ID);
                    param.Add("@Photo", employee.Photo);
                    param.Add("@Code", DateTime.Now.ToString("HHmmssFF"));

                    var result = await db.QueryFirstOrDefaultAsync<int>("EmployeesInsertTemp", param, commandType: CommandType.StoredProcedure);
                    IscompletInsert = result.ToString();

                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                IscompletInsert = ex.Message;
            }

            return IscompletInsert;
        }

        public async Task<string[]> InsertRegisterTempAsync(RegisterTemp RegisterTemp)
        {
            string IscompletInsert = null;
            string Token = Helpers.Helper.CreateToken();
            try
            {
                DynamicParameters PRegisterTemp = new DynamicParameters();
                PRegisterTemp.Add("@NameAr", RegisterTemp.NameAr);
                PRegisterTemp.Add("@IDNo", RegisterTemp.IDNo);
                PRegisterTemp.Add("@Mobile", RegisterTemp.Mobile);
                PRegisterTemp.Add("@Email", RegisterTemp.Email);
                PRegisterTemp.Add("@Gender_ID", RegisterTemp.Gender_ID);
                PRegisterTemp.Add("@Token", Token);
                await db.QueryAsync<int>("RegisterTempInsert", PRegisterTemp, commandType: CommandType.StoredProcedure);
            }
            catch (SqlException ex)
            {
                IscompletInsert = ex.Message;
            }


            string[] stringResult = new string[] { IscompletInsert, Token };

            return stringResult;
        }

        public async Task<string> GetRegisterTemp(string Token)
        {
            string IscompletInsert = null;
            try
            {
                DynamicParameters PRegisterTemp = new DynamicParameters();
                PRegisterTemp.Add("@Token", Token);

                var GetData = await db.QueryFirstOrDefaultAsync<RegisterTemp>("RegisterTempSelectByToken", PRegisterTemp, commandType: CommandType.StoredProcedure);
                if (GetData != null)
                {
                    Employee emp = new Employee();
                    emp.NameAr = GetData.NameAr;
                    emp.IDNo = GetData.IDNo;
                    emp.Mobile = GetData.Mobile;
                    emp.Email = GetData.Email;
                    emp.Gender_ID = Convert.ToByte(GetData.Gender_ID);
                    string CheckAdd = await InsertEmployeeAsync(emp);
                    IscompletInsert = CheckAdd;
                }

            }
            catch (SqlException ex)
            {
                IscompletInsert = ex.Message;
            }
            return IscompletInsert;
        }
        public async Task<RegisterTemp> SendCodeByRegisterTemp(string Token)
        {
            DynamicParameters PRegisterTemp = new DynamicParameters();
            PRegisterTemp.Add("@Token", Token);

            return await db.QueryFirstOrDefaultAsync<RegisterTemp>("RegisterTempSelectByToken", PRegisterTemp, commandType: CommandType.StoredProcedure);

        }


        public async Task<Employee> GetEmployeeByCodeAsync(int code)
        {
            param.Add("@Code", code);
            return await db.QueryFirstAsync<Employee>("EmployeesSelectByCode", param, commandType: CommandType.StoredProcedure);
        }

        public async Task<string> VerifyEmployeeAsync(int id)
        {
            string IscompletInsert = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    DynamicParameters p = new DynamicParameters();
                    p.Add("@Id", id);
                    var result = await db.QueryFirstOrDefaultAsync<string>("EmployeesVerify", p, commandType: CommandType.StoredProcedure);
                    IscompletInsert = result;
                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                IscompletInsert = ex.Message;
            }

            return IscompletInsert;
        }

        public async Task<Employee> ResendVerifyCodeAsync(int id)
        {
            var IscompletInsert = new Employee();

            using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@Id", id);
                var result = await db.QueryFirstOrDefaultAsync<Employee>("EmployeesGetVerfiyCode", p, commandType: CommandType.StoredProcedure);
                if (!result.Enabled == true)
                {
                    IscompletInsert = result;
                }
                else
                {
                    IscompletInsert = null;
                }

                transaction.Complete();
            }

            return IscompletInsert;
        }


        public async Task<Employee> EmployeesSelectByID(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@ID", id);
            return await db.QueryFirstOrDefaultAsync<Employee>("EmployeesSelectByID", p, commandType: CommandType.StoredProcedure);
        }
        public async Task<string> EmployeesUpdateDepartment(string Code, int Emp_ID)
        {
            string IscompletInsert = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    DynamicParameters p = new DynamicParameters();
                    p.Add("@Emp_ID", Emp_ID);
                    p.Add("@DepartmentCode", Code);
                    int result = await db.QueryFirstOrDefaultAsync<int>("EmployeesUpdateDepartment", p, commandType: CommandType.StoredProcedure);
                    if (result == 0)
                    {
                        IscompletInsert = "Erorr";
                    }
                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                IscompletInsert = ex.Message;
            }

            return IscompletInsert;
        }
        public async Task<string> EmpClockINOut(string Code, int EmpID, int Type)
        {
            string IscompletInsert = null;
            DateTime DateTimeNow = DateTime.UtcNow.AddHours(3);
            String date = DateTimeNow.Date.ToString("yyyy-MM-dd");

            int DateTimeH = DateTimeNow.Hour;
            int DateTimeM = DateTimeNow.Minute;
            string Time = Convert.ToString(DateTimeH) + ":" + Convert.ToString(DateTimeM);
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    DynamicParameters p = new DynamicParameters();
                    p.Add("@Emp_ID", EmpID);
                    p.Add("@User_ID", EmpID);
                    p.Add("@CheckDate", date);
                    p.Add("@CheckTime", Time);
                    p.Add("@CheckType", Type);
                    p.Add("@ShiftFrom", Time);
                    p.Add("@ShiftTo", Time);
                    int result = await db.QueryFirstOrDefaultAsync<int>("CheckInOutActionInsert", p, commandType: CommandType.StoredProcedure);
                    if (result == 0)
                    {
                        IscompletInsert = "Erorr";
                    }
                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                IscompletInsert = ex.Message;
            }

            return IscompletInsert;
        }
        /// //////////////////////////////////////
        public async Task<IEnumerable<ClockInEmployee>> GetCheckInOutActionByDate(int BranchID)
        {

            DateTime DateTimeNow = DateTime.UtcNow.AddHours(3);
            String date = DateTimeNow.Date.ToString("yyyy-MM-dd");

            int DateTimeH = DateTimeNow.Hour;
            int DateTimeM = DateTimeNow.Minute;
            string Time = Convert.ToString(DateTimeH) + ":" + Convert.ToString(DateTimeM);
            try
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@CheckDate", date);
                p.Add("@Branch_ID", BranchID);
                return await db.QueryAsync<ClockInEmployee>("GetCheckInOutActionByDate", p, commandType: CommandType.StoredProcedure);
            }
            catch (SqlException ex)
            {
                throw;
            }
        }
        /////////////////////////////////////////
        /// //////////////////////////////////////
        public async Task<ClockInEmployee> GetEmpCheckInOutActionByDate(int EmpID)
        {

            DateTime DateTimeNow = DateTime.UtcNow.AddHours(3);
            String date = DateTimeNow.Date.ToString("yyyy-MM-dd");

            int DateTimeH = DateTimeNow.Hour;
            int DateTimeM = DateTimeNow.Minute;
            string Time = Convert.ToString(DateTimeH) + ":" + Convert.ToString(DateTimeM);
            try
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@CheckDate", date);
                p.Add("@Emp_ID", EmpID);
                return await db.QueryFirstOrDefaultAsync<ClockInEmployee>("GetCheckInOutActionByEmp_ID", p, commandType: CommandType.StoredProcedure);
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        /// //////////////////////////////////////
        public async Task<string> UploadEmployeeImg(string Photo, int EmpID)
        {
            string IsCopmlete = null; 

            try
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@Photo", Photo);
                p.Add("@EmpID", EmpID);
                 await db.QueryAsync<int>("UploadEmployeeImg", p, commandType: CommandType.StoredProcedure);
            }
            catch (SqlException ex)
            {
                IsCopmlete = ex.Message;
            }

            return IsCopmlete ;
        }
        /////////////////////////////////////////
    }
}