﻿using ClockIn.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClockIn.Repositories
{
    public interface IEmployeeRepository
    {
        Task<IEnumerable<Employee>> GetAllEmployeeAsync();
        Task<string>   InsertEmployeeAsync(Employee employee);
        Task<Employee> GetEmployeeByCodeAsync(int code);
        Task<string>   VerifyEmployeeAsync(int id);
        Task<Employee> ResendVerifyCodeAsync(int id);
        Task<Employee> EmployeesSelectByID (int id);
        Task<string[]>   InsertRegisterTempAsync(RegisterTemp RegisterTemp);
        Task<string> GetRegisterTemp(string Token );
        Task<RegisterTemp> SendCodeByRegisterTemp(string Token);
        Task<string> EmployeesUpdateDepartment(string Code ,int  EmpID);
        Task<string> EmpClockINOut(string Code, int EmpID, int Type);
        Task<string> UploadEmployeeImg(string Photo, int EmpID);
        Task <IEnumerable<ClockInEmployee>> GetCheckInOutActionByDate(int BranchID);
        Task<ClockInEmployee> GetEmpCheckInOutActionByDate(int EmpID);
    }
}