﻿using ClockIn.Models;
using ClockIn.Models.ViewModel;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Transactions;
using static ClockIn.DataAccessLayer.DataAccess;

namespace ClockIn.Repositories
{
    public class DepartmentBDRepository : IDepartmentBDRepository
    {
        public IDbConnection db = new SqlConnection(GetConnectionString());

        public async Task<IEnumerable<DepartmentBranch>> DepartmentsFillListByCompanyID(int CompanyID)
        {
            try
            {
                DynamicParameters p = new DynamicParameters();
                p.Add("@CompanyID", CompanyID);
                return await db.QueryAsync<DepartmentBranch>("DepartmentFillListByCompanyID", p, commandType: CommandType.StoredProcedure);
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public async Task<string> AddNewDepartment(DepartmentViewModel department)
        {
            string isComplete = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    DynamicParameters pCompany = new DynamicParameters();
                    pCompany.Add("@NameAr", department.NameAr);
                    pCompany.Add("@NameEng", department.NameAr);
                    pCompany.Add("@Emp_ID", !string.IsNullOrEmpty(department.Emp_ID.ToString()) ? department.Emp_ID : null);
                    pCompany.Add("@LogIn_Time", department.LogIn_Time);
                    pCompany.Add("@LogOut_Time", department.LogOut_Time);
                    pCompany.Add("@Notes", !string.IsNullOrEmpty(department.Notes) ? department.Notes : null);
                    pCompany.Add("@Branch_ID", department.Branch_ID);
                    pCompany.Add("@DepartmentCode", DateTime.Now.ToString("HHmmssFF"));
                    int BranchId = await db.QueryFirstOrDefaultAsync<int>("DepartmentInsert", pCompany, commandType: CommandType.StoredProcedure);

                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                isComplete = ex.Message;
            }

            return isComplete;
        }

        public async Task<string> UpdateDepartment(DepartmentViewModel department)
        {
            string IsComplet = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    DynamicParameters pCompany = new DynamicParameters();
                    pCompany.Add("@ID", department.ID);
                    pCompany.Add("@NameAr", department.NameAr);
                    pCompany.Add("@NameEng", department.NameAr);
                    pCompany.Add("@Emp_ID", department.Emp_ID);
                    pCompany.Add("@LogIn_Time", department.LogIn_Time);
                    pCompany.Add("@LogOut_Time", department.LogOut_Time);
                    pCompany.Add("@Notes", department.Notes);
                    pCompany.Add("@Branch_ID", department.Branch_ID);
                    int BranchId = await db.QueryFirstOrDefaultAsync<int>("DepartmentUpdate", pCompany, commandType: CommandType.StoredProcedure);
                    transaction.Complete();
                }

            }
            catch (SqlException ex)
            {
                IsComplet = ex.Message;
            }

            return IsComplet;
        }

        public async Task<string> DeleteDepartment(int id)
        {
            string iscompletInsert = null;
            try
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    DynamicParameters pCompany = new DynamicParameters();
                    pCompany.Add("@ID", id);
                    await db.QueryAsync<int>("DepartmentDelete", pCompany, commandType: CommandType.StoredProcedure);
                    transaction.Complete();
                }
            }
            catch (SqlException ex)
            {
                iscompletInsert = ex.Message;
            }

            return iscompletInsert;
        }

    }
}