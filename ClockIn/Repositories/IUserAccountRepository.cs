﻿using ClockIn.DTO;
using ClockIn.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClockIn.Repositories
{
    public interface IUserAccountRepository
    {
        Task<IEnumerable<UserAccount>> GetAllUserAccountAsync();
        Task<string> InsertUserAccountAsync(UserAccount employee);
        Task<UserAccount> GetUserAccountByIdAsync(int id);
        Task<UserLoginData> LoginUserAsync(UserAccountDto userAccountDto);
        Task<UserLoginData> LoginUserPortalAsync(UserAccountDto userAccountDto);
        Task<UserLoginData> EmployeeGetAllData(int EmpID);
        Task<string> EmployeeChangPassword(changPass data); 
    }
}