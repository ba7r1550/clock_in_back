﻿using ClockIn.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClockIn.Repositories
{
    public interface IBranchBDRepository
    {
        Task<IEnumerable<Branch>> BranchesFillListByCompanyID(int CompanyID);
        Task<string> AddNewBranch(Branch branch);
        Task<string> UpdateBranch(Branch Branch);
        Task<string> DeleteBranch(int id);
    }
}