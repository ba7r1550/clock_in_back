﻿using ClockIn.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClockIn.Repositories
{
    public interface ICompanyBDRepository
    {
        Task<string> RegisterCompany(RegisterNewCompany company , int  UserName , int password );
    }
}