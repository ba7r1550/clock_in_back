﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ClockIn.Helpers
{
    public class MessageSender : IMessageSender
    {
        public async Task SendEmailAsync(string toEmail, string subject, string body)
        {
            await Execute(toEmail, subject, body);
        }

        public async Task Execute(string toEmail, string subject, string body)
        {
            try
            {
                SmtpClient client = new SmtpClient("mail.clockinhr.com");

                //If you need to authenticate
                client.Credentials = new NetworkCredential("noreply@clockinhr.com", "Noreply@1983");
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("noreply@clockinhr.com");
                mailMessage.To.Add(toEmail);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                await client.SendMailAsync(mailMessage);
            }
            catch (Exception ex)
            { }
        }
    }
}