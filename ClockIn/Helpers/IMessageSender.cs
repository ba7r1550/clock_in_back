﻿using System.Threading.Tasks;

namespace ClockIn.Helpers
{
    public interface IMessageSender
    {
        Task SendEmailAsync(string toEmail, string subject, string body);
    }
}
