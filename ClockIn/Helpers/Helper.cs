﻿using Nexmo.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ClockIn.Helpers
{
    public class Helper
    {
        public static string CreateToken()
        {
            Random rnd = new Random();
            string DateStr = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
            int RndNum = rnd.Next(1000000, 9999999);
            string HashLast = DateStr + "asdfghjkl;qwertyuiopzxcvbnm,asdfghjklqwertyuiopasdfghjklzxcvbnm" + Convert.ToString(RndNum);

            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(HashLast));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }

            return hash.ToString();
        }
        public static string CreateBarCode()
        {
            string CustomCode = DateTime.Now.ToString("HHmmssFFFFFF");
            return CustomCode;
        }
        public static DateTime ConvertDate(string str)
        {
            DateTimeOffset dto = DateTimeOffset.Parse(str);
            //Get the date object from the string. 
            DateTime dtObject = dto.DateTime;
            //Convert the DateTimeOffSet to string. 
            string newVal = dto.ToString("d");
            DateTime TrDate = Convert.ToDateTime(newVal);
            return TrDate;
        }

        public static int RandNum() 
        {
            Random rnd = new Random();
            int Mynum  = rnd.Next(1000000, 9999999);  // creates a number between 1 and 12
            return Mynum; 
        }

        public static bool SendSms(int code , string PhoneNum )
        {
            try
            {
                var client = new Client(creds: new Nexmo.Api.Request.Credentials
                {
                    ApiKey = "27362ac4",
                    ApiSecret = "ShcniwXPsR5LOPE8"
                });

                var results = client.SMS.Send(request: new SMS.SMSRequest
                {

                    from = "ClockIn ",
                    to = PhoneNum,
                    text = "Your OPT IS : " + Convert.ToString(code)
                }); ;



                return true;
            }
            catch (Exception)
            {

                return  false;
            }
            
        }
    }
}