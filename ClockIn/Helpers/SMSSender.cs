﻿using Nexmo.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClockIn.Helpers
{
    public class SMSSender
    {
        public static Client GetClient()
        {
            const string API_KEY = "27362ac4";
            const string API_SECRET = "ShcniwXPsR5LOPE8";

            var client = new Client(creds: new Nexmo.Api.Request.Credentials(
                nexmoApiKey: API_KEY, nexmoApiSecret: API_SECRET));
            return client;
        }

        public static SMS.SMSResponse SendSMS(string TO_NUMBER, string NEXMO_BRAND_NAME  , string message)
        {
            var client = GetClient();

            var results = client.SMS.Send(new SMS.SMSRequest
            {
                from = NEXMO_BRAND_NAME,
                to = TO_NUMBER,
                text = message
            });

            return results;
        }
    }
}