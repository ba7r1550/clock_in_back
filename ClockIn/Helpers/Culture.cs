﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;

namespace ClockIn.Helpers
{
    public class Culture : ICulture
    {
        public void SetCulture(string lang = "1")
        {
            var langId = lang == "1" ? "ar-SA" : string.IsNullOrEmpty(lang) ? "ar-SA" : "en-US";

            HttpContext.Current.Response.Cookies.Remove("Language");

            var languageCookie = HttpContext.Current.Request.Cookies["Language"];

            if (languageCookie == null) languageCookie = new HttpCookie("Language");

            languageCookie.Value = langId;

            languageCookie.Expires = DateTime.Now.AddDays(10);

            HttpContext.Current.Response.SetCookie(languageCookie); ;

            Thread.CurrentThread.CurrentCulture = new CultureInfo(languageCookie.Value);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageCookie.Value);
        }
    }
}