﻿namespace ClockIn.Helpers
{
    public interface ICulture
    {
        void SetCulture(string lang);
    }
}
