﻿using System.ComponentModel.DataAnnotations;

namespace ClockIn.DTO
{
    public class EmployeeDto
    {
        [Required]
        [StringLength(250)]
        public string NameAr { get; set; }

        [Required]
        [StringLength(20)]
        public string IDNo { get; set; }

        [Required]
        [StringLength(20)]
        public string Mobile { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        public byte? Gender_ID { get; set; }
    }
}