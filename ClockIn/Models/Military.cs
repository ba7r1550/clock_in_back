using System.ComponentModel.DataAnnotations;

namespace ClockIn.Models
{
    public class Military
    {
        public byte ID { get; set; }

        [StringLength(25)]
        public string NameAr { get; set; }

        [StringLength(25)]
        public string NameEng { get; set; }
    }
}
