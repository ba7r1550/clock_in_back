using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClockIn.Models
{
    public class City
    {
        //public City()
        //{
        //    Employees = new HashSet<Employee>();
        //}

        public int ID { get; set; }

        [StringLength(150)]
        public string NameAr { get; set; }

        [StringLength(150)]
        public string NameEng { get; set; }

        public short? State_ID { get; set; }

        [StringLength(300)]
        public string Notes { get; set; }

        public State State { get; set; }

        //public ICollection<Employee> Employees { get; set; }
    }
}
