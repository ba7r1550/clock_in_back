using System.ComponentModel.DataAnnotations;

namespace ClockIn.Models
{
    public class Gender
    {
        public byte ID { get; set; }

        [StringLength(10)]
        public string NameAr { get; set; }

        [StringLength(10)]
        public string NameEng { get; set; }
    }
}
