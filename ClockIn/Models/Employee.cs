using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClockIn.Models
{
    public class Employee
    {
        //public Employee()
        //{
        //    Branches = new HashSet<Branch>();
        //    Departments = new HashSet<Department>();
        //    UserAccounts = new HashSet<UserAccount>();
        //}

        public int ID { get; set; }

        [StringLength(250)]
        public string NameAr { get; set; }

        [StringLength(250)]
        public string NameEng { get; set; }

        public byte? Gender_ID { get; set; }

        [StringLength(20)]
        public string IDNo { get; set; }

        [StringLength(80)]
        public string Address { get; set; }

        [StringLength(20)]
        public string Tel { get; set; }

        [StringLength(20)]
        public string Mobile { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public short? Job_ID { get; set; }

        public double? Salary { get; set; }

        public short? Dep_ID { get; set; }

        [StringLength(50)]
        public string SocialID { get; set; }

        public byte? Marital_Status { get; set; }

        public byte? Millitary_ID { get; set; }

        public bool? Enabled { get; set; }

        public short? QualifType_ID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? BirthDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Employment_Date { get; set; }

        public short? GraduateYear { get; set; }

        public int? City_ID { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

        public bool? DriverLicense { get; set; }

        public byte? DriverLicense_Type { get; set; }

        [StringLength(50)]
        public string DriverLicenseNo { get; set; }

        public short? Branch_ID { get; set; }

        public string Photo { get; set; }

        public int Code { get; private set; } = (new Random().Next(10000000) + 1);

        //public ICollection<Branch> Branches { get; set; }

        //public Branch Branch { get; set; }

        //public City City { get; set; }

        //public ICollection<Department> Departments { get; set; }

        //public Job Job { get; set; }

        //public ICollection<UserAccount> UserAccounts { get; set; }
    }

    public class ClockInEmployee 
    {
        public string Code { get; set; }
        public string  EmpNameAr { get; set; }
        public string  EmpNameEng { get; set; }
        public string  CheckType { get; set; }
        public string CheckDate { get; set; }
        public string CheckTime_Am { get; set; }
        public TimeSpan  CheckTime { get; set; }
        public string  CheckID { get; set; }
        public bool  CheckInAgain { get; set; }
    }
}
