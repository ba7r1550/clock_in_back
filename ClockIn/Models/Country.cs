using System.ComponentModel.DataAnnotations;

namespace ClockIn.Models
{
    public class Country
    {
        public short ID { get; set; }

        [StringLength(60)]
        public string NameAr { get; set; }

        [StringLength(60)]
        public string NameEng { get; set; }

        [StringLength(10)]
        public string TelCode { get; set; }

        [StringLength(300)]
        public string Notes { get; set; }
    }
}
