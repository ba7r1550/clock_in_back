﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClockIn.Models
{
    public class CompanyBD
    {

    }

    public class RegisterNewCompany
    {
        public string NameAr { get; set; }
        public string NameEng { get; set; }
        public string Address { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string CompType_ID { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public int? Country_ID { get; set; }
        public int? City_ID { get; set; }
        public int? SaveUser_ID { get; set; }
        public string SaveDate { get; set; }
        public string User_Name { get; set; }
        public string User_PWD { get; set; }
        public string DepartmentCode { get; set; }
    }
}