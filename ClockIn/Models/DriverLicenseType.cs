using System.ComponentModel.DataAnnotations;

namespace ClockIn.Models
{
    public class DriverLicenseType
    {
        public short ID { get; set; }

        [StringLength(250)]
        public string NameAr { get; set; }

        [StringLength(250)]
        public string NameEng { get; set; }

        [StringLength(300)]
        public string Notes { get; set; }
    }
}
