﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClockIn.Models
{
    public class RegisterTemp
    {
        public string NameAr    { get; set; }
        public string IDNo      { get; set; }
        public string Mobile    { get; set; }
        public string Email     { get; set; }
        public string Gender_ID { get; set; }
        public string Token     { get; set; }
    }
}