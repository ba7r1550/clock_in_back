using System.ComponentModel.DataAnnotations;

namespace ClockIn.Models
{
    public class UserAccount
    {
        public int ID { get; set; }

        public int? Emp_ID { get; set; }

        [StringLength(25)]
        public string User_Name { get; set; }

        [StringLength(25)]
        public string User_PWD { get; set; }

        public string Emp_NameAr { get; set; }

        public string Emp_NameEng { get; set; }

        public bool? Enabled { get; set; }

        //public Employee Employee { get; set; }
    }
    public class UserLoginData 
    {
        public int? Emp_ID { get; set; }
        public string Emp_NameAr { get; set; }
        public string Emp_NameEng { get; set; }
        public string Company_ID { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public string Mobile { get; set; }
        public string Dep_ID { get; set; }

    }

    public class changPass
    {
        public int EmpID { get; set; }
        public string Password { get; set; }
    }
}
