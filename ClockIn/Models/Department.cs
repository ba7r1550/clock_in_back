using System;
using System.ComponentModel.DataAnnotations;

namespace ClockIn.Models
{
    public class Department
    {
        public short ID { get; set; }

        [StringLength(50)]
        public string NameAr { get; set; }

        [StringLength(50)]
        public string NameEng { get; set; }

        public int? Emp_ID { get; set; }

        public TimeSpan? LogIn_Time { get; set; }

        public TimeSpan? LogOut_Time { get; set; }

        [StringLength(300)]
        public string Notes { get; set; }

        public Employee Employee { get; set; }
    }

    public class DepartmentBranch 
    {
        public string DepartmentID  { get; set; }
        public string BranchesNameAr { get; set; }
        public string BranchesNameEng { get; set; }
        public string DepartmentNameAr { get; set; }
        public string DepartmentNameEng { get; set; }
        public string DepartmentCode { get; set; }
        public TimeSpan? LogIn_Time { get; set; }
        public TimeSpan? LogOut_Time { get; set; }
        public int Branch_ID { get; set; }
    }
}
