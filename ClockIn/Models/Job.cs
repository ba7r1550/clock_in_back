using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClockIn.Models
{
    public class Job
    {
        public Job()
        {
            Employees = new HashSet<Employee>();
        }

        public short ID { get; set; }

        [StringLength(250)]
        public string NameAr { get; set; }

        [StringLength(250)]
        public string NameEng { get; set; }

        [StringLength(300)]
        public string Notes { get; set; }

        public ICollection<Employee> Employees { get; set; }
    }
}
