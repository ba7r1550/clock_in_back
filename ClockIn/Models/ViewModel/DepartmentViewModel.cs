﻿using System;

namespace ClockIn.Models.ViewModel
{
    public class DepartmentViewModel
    {
        public int ID { get; set; }
        public string NameAr { get; set; }

        public string NameEng { get; set; }

        public int? Emp_ID { get; set; }

        public TimeSpan? LogIn_Time { get; set; }

        public TimeSpan? LogOut_Time { get; set; }

        public string Notes { get; set; }

        public int Branch_ID { get; set; }
        public int Dep_ID { get; set; }
        public string DepartmentCode { get; set; }
    }
}