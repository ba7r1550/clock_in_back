using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClockIn.Models
{
    public class Branch
    {
        //public Branch()
        //{
        //    Employees = new HashSet<Employee>();
        //}

        public short ID { get; set; }

        [StringLength(100)]
        public string NameAr { get; set; }

        [StringLength(100)]
        public string NameEng { get; set; }

        [StringLength(300)]
        public string Address { get; set; }

        public int? Emp_ID { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(300)]
        public string Notes { get; set; }

        public int? City_ID { get; set; }

        public int? SaveUser_ID { get; set; }

        public DateTime? SaveDate { get; set; }

        public int? LastEditUser_ID { get; set; }

        public DateTime? LastEditDate { get; set; }

        [StringLength(50)]
        public string Latitude { get; set; }
        [StringLength(50)]
        public string Longitude { get; set; }
        public int? Company_ID { get; set; }

        //public Employee Employee { get; set; }

        //public ICollection<Employee> Employees { get; set; }
    }
}
