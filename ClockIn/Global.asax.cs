using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ClockIn
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var cors = new EnableCorsAttribute(origins: "*", headers: "*", methods: "*");
            GlobalConfiguration.Configuration.EnableCors(cors);
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);  
        }
    }
}
