﻿using ClockIn.Repositories;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace ClockIn.Controllers
{
    public class GenderController : ApiController
    {
        [HttpGet]
        [Route("api/gender/get-all-gender/")]
        public async Task<APIResponse> GetAllGendersAsync()
        {
            IGenderRepository genderRepository = new GenderRepository();
            var CheckData = await genderRepository.GetAllGenderAsync();
            if (CheckData.Count() >= 0)
            {
                var result = new { Message = Resourse.ClockInPro.Success, Data = CheckData };
                return new APIResponse(201, result: result);
            }
            else
            {
                throw new ApiException("Failed to get data", 500);
            }
        }
    }
}