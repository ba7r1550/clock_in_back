﻿using ClockIn.Helpers;
using ClockIn.Models;
using ClockIn.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace ClockIn.Controllers
{

    public class CompanyController : ApiController
    {

        [HttpPost]
        [Route("api/company/register-company")]
        public async Task<IHttpActionResult> RegisterCompany(RegisterNewCompany company)
        {

            ICulture culture = new Culture();
            culture.SetCulture("2");
            ICompanyBDRepository CompanyDbRep = new CompanyBDRepository();
            /////////////////////////////////////////////////////////////////////////////////
            int UserName = Convert.ToInt32(DateTime.Now.ToString("HHmmssFF"));
            int password = Helper.RandNum();
            /////////////////////////////////////////////////////////////////////////////////
            string added = await CompanyDbRep.RegisterCompany(company, UserName, password);
            if (added == null)
            {

                IMessageSender messageSender = new MessageSender();
                await messageSender.SendEmailAsync(company.Email, Resourse.ClockInPro.VerifySubject, string.Format(Resourse.ClockInPro.VerifyBody, UserName, password));


                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                return Json(result);
            }
            else
            {
                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }



        [HttpGet]
        [Route("api/company/get-emp-today/{BranchID}")]
        public async Task<IHttpActionResult> GetCheckInOutActionByDate(int BranchID)
        {
            ICulture culture = new Culture();
            culture.SetCulture("2");
            IEmployeeRepository employeeRepository = new EmployeeRepository();
            var Emp = await employeeRepository.GetCheckInOutActionByDate(BranchID);
            if (Emp.Any())
            {


                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved, data = Emp };
                return Json(result);
            }
            else
            {
                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }
        [HttpGet]
        [Route("api/company/get-emp-last-today/{EmpID}/{CheckAgineTest?}")]
        public async Task<IHttpActionResult> GetEmpCheckInOutActionByDate(int EmpID , bool CheckAgineTest = false)
        {
            ICulture culture = new Culture();
            culture.SetCulture("2");
            IEmployeeRepository employeeRepository = new EmployeeRepository();
            var Emp = await employeeRepository.GetEmpCheckInOutActionByDate(EmpID);
            if (Emp !=null)
            {

                Emp.CheckInAgain = CheckAgineTest; 
                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved, data = Emp };
                return Json(result);
            }
            else
            {
                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }
    }
}