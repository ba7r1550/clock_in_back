﻿using ClockIn.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ClockIn.Controllers
{
    public class FileUploadController : ApiController
    {
        [HttpPost]
        [Route("api/upload-file-account/")]
        public async Task<IHttpActionResult> UploadFileAccount()
        {
            var fileMasterName = "";
            var fileUploadedName = "";
            int EmpID;
            var domainName = HttpContext.Current.Request.Url.Host + "/FileUpload/";
            string checkex  = null ; 
            var httpRequest = HttpContext.Current.Request;
            try
            {
                string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };
                bool isValidFile = false;

                var Exupload = new { Result = false, Message = "This Is Extension Not Allow  " };
                if (httpRequest.Files.Count > 0)
                {
                    EmpID = Convert.ToInt32(httpRequest.Form.Get("EmpID"));

                    var docfiles = new List<string>();
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        fileMasterName = postedFile.FileName;
                        string ext = System.IO.Path.GetExtension(postedFile.FileName);
                        checkex = ext; 
                        ////////////////////////////////////////////////////////////////////////////

                        for (int i = 0; i < validFileTypes.Length; i++)
                        {
                            if (ext == "." + validFileTypes[i])
                            {
                                isValidFile = true; 
                            }
                        }

                        if (isValidFile == false) 
                        {
                            
                            return Json(Exupload);
                        }
                        ////////////////////////////////////////////////////////////////////////////

                        fileUploadedName = DateTime.Now.ToString("yyyyMMddHHmmssFFF") + "_" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath("~/FileUpload/" + fileUploadedName);
                        postedFile.SaveAs(filePath);
                        docfiles.Add(filePath);
                    }
                    string FileUploadedName = "http://"+domainName+fileUploadedName; 
                    IEmployeeRepository employeeRepository = new EmployeeRepository();
                    string check = await employeeRepository.UploadEmployeeImg(FileUploadedName , EmpID);
                   
                    if (check == null )
                    {
                        var result = new { FileMasterName = fileMasterName, FileUploadedName = FileUploadedName, checkex = checkex , EmpID = EmpID  };
                        return Json(result);

                    }
                    else
                    {
                        var result = new { Result = false, Message = "Cant File Upload " };
                        return Json(result);
                    }
                }
                else
                {
                    var result = new { Result = false, Message = "Cant File Upload " };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                var result = new { Result = false, Message = ex.Message };
                return Json(result);

            }
        }
    }
}
