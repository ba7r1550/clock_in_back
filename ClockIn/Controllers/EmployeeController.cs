﻿using ClockIn.DTO;
using ClockIn.Helpers;
using ClockIn.Models;
using ClockIn.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace ClockIn.Controllers
{
    public class EmployeeController : ApiController
    {
        [HttpGet]
        [Route("api/employee/get-all-employee/")]
        public async Task<APIResponse> GetAllEmployeesAsync()
        {
            IEmployeeRepository employeeRepository = new EmployeeRepository();
            var CheckData = await employeeRepository.GetAllEmployeeAsync();
            if (CheckData.Count() >= 0)
            {
                var result = new { Message = Resourse.ClockInPro.Success, Data = CheckData };
                return new APIResponse(201, result: result);
            }
            else
            {
                throw new ApiException("Failed to get data", 500);
            }
        }

        [HttpPost]
        [Route("api/employee/register-new-employee/{langId?}")]
        public async Task<IHttpActionResult> NewEmployeeAsync(RegisterTemp employee, string langId = null)
        {
            try
            {
                ICulture culture = new Culture();
                culture.SetCulture(langId);



                IEmployeeRepository employeeRepository = new EmployeeRepository();
                IUserAccountRepository userAccountRepository = new UserAccountRepository();
                int RandomCode = Helper.RandNum();

                string[] CompleteData = await employeeRepository.InsertRegisterTempAsync(employee);

                if (CompleteData[0] == null)
                {
                    string PhoneNumber = employee.Mobile;
                    string From = "ClockIn";
                    string Msg = "Your OTP IS : " + Convert.ToString(RandomCode);

                    var CheckSend = SMSSender.SendSMS(PhoneNumber, From, Msg);
                    var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved, Code = RandomCode, Token = CompleteData[1] };
                    return Json(result);
                }

                else
                {
                    var result = new { Result = false, Message = CompleteData[0] };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                string issues = ex.Message;
                var result = new { Result = false, Message = issues };
                return Json(result);

            }
        }

        [HttpGet]
        [Route("api/employee/verify-employee/{langId?}/{Token}")]
        public async Task<IHttpActionResult> VerifyEmployeeAsync(string Token, string langId = null)
        {
            IEmployeeRepository employeeRepository = new EmployeeRepository();
            IUserAccountRepository userAccountRepository = new UserAccountRepository();
            string CheckVerify = await employeeRepository.GetRegisterTemp(Token);
            if (CheckVerify != null)
            {
                ICulture culture = new Culture();
                culture.SetCulture(langId);

                // updateing Employee data
                var CheckData = await employeeRepository.EmployeesSelectByID(Convert.ToInt32(CheckVerify));
                var newuser = new UserAccount
                {
                    Emp_ID = Convert.ToInt32(CheckVerify),
                    User_Name = CheckData.IDNo,
                    User_PWD = (new Random().Next(10000000) + 1).ToString(),
                    Enabled = false
                };

                await userAccountRepository.InsertUserAccountAsync(newuser);
                // send user info by mail
                var user = await userAccountRepository.GetUserAccountByIdAsync(CheckData.ID);

                IMessageSender messageSender = new MessageSender();
                await messageSender.SendEmailAsync(CheckData.Email, Resourse.ClockInPro.VerifySubject,
                                                                   string.Format(Resourse.ClockInPro.VerifyBody, user.User_Name, user.User_PWD));

                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                return Json(result);
            }
            else
            {
                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }

        [HttpGet]
        [Route("api/employee/resend-verify-employee/{langId?}/{Token}")]
        public async Task<IHttpActionResult> ResendVerifyCodeAsync(string Token, string langId = null)
        {
            IEmployeeRepository employeeRepository = new EmployeeRepository();
            var CheckData = await employeeRepository.SendCodeByRegisterTemp(Token);
            if (CheckData != null)
            {
                ICulture culture = new Culture();
                culture.SetCulture(langId);
                int RandomCode = Helper.RandNum();
                string PhoneNumber = CheckData.Mobile;
                string From = "ClockIn";
                string Msg = "Your OTP IS : " + Convert.ToString(RandomCode);

                var CheckSend = SMSSender.SendSMS(PhoneNumber, From, Msg);
                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved, Code = RandomCode, Token = Token };
                return Json(result);
            }
            else
            {
                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }

        [HttpPost]
        [Route("api/employee/login/{langId?}")]
        public async Task<IHttpActionResult> LoginAsync(UserAccountDto userAccountDto, string langId = null)
        {
            try
            {
                ICulture culture = new Culture();
                culture.SetCulture(langId);

                if (string.IsNullOrEmpty(userAccountDto.User_Name))
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.UserNameRequired };
                    return Json(result);
                }
                if (string.IsNullOrEmpty(userAccountDto.User_PWD))
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.PasswordRequired };
                    return Json(result);
                }

                IUserAccountRepository userAccountRepository = new UserAccountRepository();
                var IscompletInsert = await userAccountRepository.LoginUserAsync(userAccountDto);

                if (IscompletInsert != null)
                {
                    var result = new { Result = true, Message = Resourse.ClockInPro.LoginOk, data = IscompletInsert };
                    return Json(result);
                }
                else
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.LoginError };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                var result = new { Result = false, Message = ex.Message };
                return Json(result);
            }
        }

        [HttpPost]
        [Route("api/employee/login-portal/{langId?}")]
        public async Task<IHttpActionResult> LoginPortalAsync(UserAccountDto userAccountDto, string langId = null)
        {
            try
            {
                ICulture culture = new Culture();
                culture.SetCulture(langId);

                if (string.IsNullOrEmpty(userAccountDto.User_Name))
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.UserNameRequired };
                    return Json(result);
                }
                if (string.IsNullOrEmpty(userAccountDto.User_PWD))
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.PasswordRequired };
                    return Json(result);
                }

                IUserAccountRepository userAccountRepository = new UserAccountRepository();
                var IscompletInsert = await userAccountRepository.LoginUserPortalAsync(userAccountDto);

                if (IscompletInsert != null)
                {
                    var result = new { Result = true, Message = Resourse.ClockInPro.LoginOk, data = IscompletInsert };
                    return Json(result);
                }
                else
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.LoginError };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                var result = new { Result = false, Message = ex.Message };
                return Json(result);
            }
        }
        [HttpGet]
        [Route("api/employee/employee-profile/{langId?}/{EmpID}")]
        public async Task<IHttpActionResult> EmployeeGetAllData(int EmpID, string langId = null)
        {
            try
            {
                ICulture culture = new Culture();
                culture.SetCulture(langId);

                IUserAccountRepository userAccountRepository = new UserAccountRepository();
                var EmployeeData = await userAccountRepository.EmployeeGetAllData(EmpID);

                if (EmployeeData != null)
                {
                    var result = new { Result = true, Message = Resourse.ClockInPro.Success, data = EmployeeData };
                    return Json(result);
                }
                else
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                var result = new { Result = false, Message = ex.Message };
                return Json(result);
            }
        }
        [HttpGet]
        [Route("api/employee/department-code/{langId?}/{Code}/{EmpID}")]
        public async Task<IHttpActionResult> DepartmentCode(string Code, int EmpID, string langId = null)
        {

            ICulture culture = new Culture();
            culture.SetCulture(langId);
            IEmployeeRepository employeeRepository = new EmployeeRepository();
            string CheckAdd = await employeeRepository.EmployeesUpdateDepartment(Code, EmpID);
            if (CheckAdd == null)
            {
                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                return Json(result);
            }
            else
            {

                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }

        }
        [HttpGet]
        [Route("api/employee/emp-clock-in-out/{langId?}/{EmpID}/{Code}/{Type}")]
        public async Task<IHttpActionResult> EmpClockINOut(string Code, int EmpID, int Type, string langId = null)
        {
            ICulture culture = new Culture();
            culture.SetCulture("2");
            IEmployeeRepository employeeRepository = new EmployeeRepository();

            string CheckAdd = await employeeRepository.EmpClockINOut(Code, EmpID, Type);
            if (CheckAdd == null)
            {
                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                return Json(result);
            }
            else
            {

                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }
        [HttpPost]
        [Route("api/employee/employee-change-password/{langId?}")]
        public async Task<IHttpActionResult> EmployeeChangPassword(changPass data, string langId = null)
        {
            try
            {
                ICulture culture = new Culture();
                culture.SetCulture(langId);

                IUserAccountRepository userAccountRepository = new UserAccountRepository();
                string IscompletInsert = await userAccountRepository.EmployeeChangPassword(data);

                if (IscompletInsert == null)
                {
                    var result = new { Result = true, Message = Resourse.ClockInPro.Success, data = IscompletInsert };
                    return Json(result);
                }
                else
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                var result = new { Result = false, Message = ex.Message };
                return Json(result);
            }
        }
    }
}
