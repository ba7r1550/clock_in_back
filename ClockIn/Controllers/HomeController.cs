﻿using System.Web.Mvc;

namespace ClockInPro.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return Content("Clock IN ");
        }
    }
}