﻿using ClockIn.Helpers;
using ClockIn.Models;
using ClockIn.Models.ViewModel;
using ClockIn.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace ClockIn.Controllers
{
    public class DepartmentController : ApiController
    {
        [HttpGet]
        [Route("api/department/get-department/{CompanyID}")]
        public async Task<IHttpActionResult> DepartmentFillListByCompanyID(int companyID)
        {
            ICulture culture = new Culture();
            culture.SetCulture("2");
            IDepartmentBDRepository CompanyDbRep = new DepartmentBDRepository();
            var Department = await CompanyDbRep.DepartmentsFillListByCompanyID(companyID);
            if (Department.Any())
            {


                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved, data = Department };
                return Json(result);
            }
            else
            {
                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }

        [HttpPost]
        [Route("api/department/new-department")]
        public async Task<IHttpActionResult> AddDepartment(DepartmentViewModel department)
        {
            ICulture culture = new Culture();
            culture.SetCulture("2");
            IDepartmentBDRepository branchRepo = new DepartmentBDRepository();
            string added = await branchRepo.AddNewDepartment(department);
            if (added == null)
            {
                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                return Json(result);
            }
            else
            {
                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }

        [HttpPost]
        [Route("api/department/update-department/")]
        public async Task<IHttpActionResult> UpdateDepartment(DepartmentViewModel department)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICulture culture = new Culture();
                    culture.SetCulture("2");

                    IDepartmentBDRepository branchRepo = new DepartmentBDRepository();
                    string IsComplete = await branchRepo.UpdateDepartment(department);

                    if (IsComplete == null)
                    {
                        var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                        return Json(result);
                    }
                    else
                    {
                        var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                        return Json(result);
                    }
                }
                else
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                string issues = ex.Message;
                var result = new { Result = false, Message = issues };
                return Json(result);
            }
        }

        [HttpGet]
        [Route("api/department/delete-department/{id}")]
        public async Task<IHttpActionResult> DeleteDepartment(int id)
        {
            try
            {
                ICulture culture = new Culture();
                culture.SetCulture("2");

                IDepartmentBDRepository branchRepo = new DepartmentBDRepository();
                var IsComplete = await branchRepo.DeleteDepartment(id);

                if (IsComplete == null)
                {
                    var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                    return Json(result);
                }
                else
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {

                string issues = ex.Message;
                var result = new { Result = false, Message = issues };
                return Json(result);
            }
        }
    }
}