﻿using ClockIn.Helpers;
using ClockIn.Models;
using ClockIn.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace ClockIn.Controllers
{
    public class BranchController : ApiController
    {
        [HttpGet]
        [Route("api/branch/get-branches/{companyID}")]
        public async Task<IHttpActionResult> BranchesFillListByCompanyID(int companyID)
        {
            ICulture culture = new Culture();
            culture.SetCulture("2");
            IBranchBDRepository branchRepo = new BranchBDRepository();
            var Branches = await branchRepo.BranchesFillListByCompanyID(companyID);
            if (Branches.Any())
            {
                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved, data = Branches };
                return Json(result);
            }
            else
            {
                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }

        [HttpPost]
        [Route("api/branch/new-branch")]
        public async Task<IHttpActionResult> AddBranch(Branch branch)
        {
            ICulture culture = new Culture();
            culture.SetCulture("2");
            IBranchBDRepository branchRepo = new BranchBDRepository();
            string added = await branchRepo.AddNewBranch(branch);
            if (added == null)
            {
                var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                return Json(result);
            }
            else
            {
                var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                return Json(result);
            }
        }

        [HttpPost]
        [Route("api/branch/update-branch/")]
        public async Task<IHttpActionResult> UpdateBranch(Branch branch)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICulture culture = new Culture();
                    culture.SetCulture("2");

                    IBranchBDRepository branchRepo = new BranchBDRepository();
                    string IsComplete = await branchRepo.UpdateBranch(branch);

                    if (IsComplete == null)
                    {
                        var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                        return Json(result);
                    }
                    else
                    {
                        var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                        return Json(result);
                    }
                }
                else
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                string issues = ex.Message;
                var result = new { Result = false, Message = issues };
                return Json(result);
            }
        }

        [HttpGet]
        [Route("api/branch/delete-branch/{id}")]
        public async Task<IHttpActionResult> DeleteBranch(int id)
        {
            try
            {
                ICulture culture = new Culture();
                culture.SetCulture("2");

                IBranchBDRepository branchRepo = new BranchBDRepository();
                var IsComplete = await branchRepo.DeleteBranch(id);

                if (IsComplete == null)
                {
                    var result = new { Result = true, Message = Resourse.ClockInPro.DataSaved };
                    return Json(result);
                }
                else
                {
                    var result = new { Result = false, Message = Resourse.ClockInPro.FieldData };
                    return Json(result);
                }
            }
            catch (Exception ex)
            {

                string issues = ex.Message;
                var result = new { Result = false, Message = issues };
                return Json(result);
            }
        }
    }
}