﻿using System.Configuration;

namespace ClockIn.DataAccessLayer
{
    public static class DataAccess
    {
        public static string GetConnectionString(string connectionName = "ClockInPro")
        {
            return ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
        }

    }
}